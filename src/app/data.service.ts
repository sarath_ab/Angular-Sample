import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class DataService {

  constructor(public http: Http) { }
  getPosts(api) {
    return this.http.get('http://jsonplaceholder.typicode.com/' + api)
        .map(res => res.json());
  }
}
