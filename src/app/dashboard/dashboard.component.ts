import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { DataService } from '../data.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
posts: Post[];
  constructor(private user: UserService, private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getPosts('posts').subscribe((posts) => {
// console.log(posts);
        this.posts = posts;
    });
  }

}

interface Post {
  id: number;
  title: string;
  body: string;
  userId: number;
}
