import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { FooterComponent } from './footer/footer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RouterModule, Routes} from '@angular/router';
import { AuthguardGuard } from './authguard.guard';
import { UserService } from './user.service';
import { DataService } from './data.service';
import { TabsComponent } from './tabs/tabs.component';
import { UserComponent } from './user/user.component';

const appRoutes: Routes = [
    {
      path: '',
      component: LoginFormComponent
    },
    {
        path: 'dashboard',
        canActivate: [AuthguardGuard],
        component: DashboardComponent
    },
    {
        path: 'user',
        canActivate: [AuthguardGuard],
        component: UserComponent
    }
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginFormComponent,
    FooterComponent,
    DashboardComponent,
    TabsComponent,
    UserComponent
  ],
  imports: [
      RouterModule.forRoot(appRoutes),
    BrowserModule,
    AppRoutingModule,
    HttpModule
  ],
  providers: [UserService, AuthguardGuard, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
